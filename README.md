# Setup

Bundle gems:

```
bundle install
```

Install yarn:

```
npm install -g yarn
```

Install dependencies

```
yarn install
```

# Running the app

Run the project:

```
npm run dev
```

View in browser:

```
http://localhost:3000
```

# Running the web tests

Run jest:

```
npm run js-tests
```

# Running the backend specs

Run rspec:

```
npm run tests
```

# Technologies

- React
- Redux pattern with `react-redux` bindings
- ES6 / Babel
- Lodash
- Redux connected `react-router`
- Airbnb EsLint configuration
- CSS modules using `css-loader`, `style-loader`, and `sass-loader`
- Uses `node-sass` for SASS compilation
- Sourcemaps using `source-map` and Webpack `source-map-loader`
- Redux action logging with `redux-logger`
- Hot module replacement / live reloading using `react-hot-loader` v3
- Postcss with `autoprefixer`
