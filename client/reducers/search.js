import immutable from 'seamless-immutable';
import { ACTION_TYPES } from 'constants/action-types';

const initialState = immutable({
  isLoading: false,
  venues: [],
  error: {}
});

export default function app(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.SEARCH.SEARCH_REQUEST:
      return state.set('isLoading', true);
    case ACTION_TYPES.SEARCH.SEARCH_SUCCESS:
      return state
              .set('venues', action.venues)
              .set('isLoading', false);
    case ACTION_TYPES.SEARCH.SEARCH_FAILURE:
      return state
              .set('isLoading', false)
              .set('error', action.error);
    default:
      return state;
  }
};
