import { ACTION_TYPES } from 'constants/action-types';

const initialState = {
  loaded: false,
};

export default function app(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.APP_LOAD:
      return { ...state, loaded: true };
    default:
      return state;
  }
}