import immutable from 'seamless-immutable';
import { ACTION_TYPES } from 'constants/action-types';

const initialState = immutable({
  latlng: undefined,
  isLoading: false
});

export default function app(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.GEOCODE.GEOCODE_REQUEST:
      return state.set('isLoading', true);
    case ACTION_TYPES.GEOCODE.GEOCODE_SUCCESS:
      return state
              .set('latlng', action.coordinates)
              .set('isLoading', false);
    case ACTION_TYPES.GEOCODE.GEOCODE_FAILURE:
      return state
              .set('isLoading', false)
              .set('error', action.error);
    default:
      return state;
  }
};
