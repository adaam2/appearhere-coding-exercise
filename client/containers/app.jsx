import React from 'react';
import { connect }  from 'react-redux';

import { loadApp } from 'actions/app';

import Map from '../components/map';

import styles from './app.scss';

type Props = {
  dispatch: () => void,
  loaded: boolean
}

class App extends React.Component {
  props: Props;

  componentDidMount() {
    this.props.dispatch(loadApp());
  }

  render() {
    return (
      <div className={styles.container}>
        <Map/>
      </div>
    )
  }
}

export default connect((store, props) => {
  return {
    loaded: store.app.loaded,
  };
})(App);