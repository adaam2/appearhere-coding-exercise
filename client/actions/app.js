import { ACTION_TYPES } from '../constants/action-types';

export function loadApp() {
  return {
    type: ACTION_TYPES.APP_LOAD
  };
}

export default { loadApp };