import { ACTION_TYPES } from '../constants/action-types';
import { default as ApiClient } from '../shared/api-client';

export const geocodeRequest = () => {
  return {
    type: ACTION_TYPES.GEOCODE.GEOCODE_REQUEST
  };
};

export const geocodeSuccess = (coordinates) => {
  return {
    type: ACTION_TYPES.GEOCODE.GEOCODE_SUCCESS,
    coordinates
  };
};

export const geocodeFailure = (error) => {
  return {
    type: ACTION_TYPES.GEOCODE.GEOCODE_FAILURE,
    error
  };
};

export const geocode = (postcode) => {
  return (dispatch) => {
    dispatch(geocodeRequest());
    return new Promise((resolve, reject) => {
      return ApiClient.post(`geocode?postcode=${postcode}`)
        .then(
          response => {
            const coordinates = response.data;
            dispatch(geocodeSuccess(coordinates));
            resolve(coordinates);
          },
          error => {
            dispatch(geocodeFailure(error));
          }
        );
    });
  };
};