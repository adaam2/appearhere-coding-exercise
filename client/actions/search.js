import { ACTION_TYPES } from '../constants/action-types';
import { default as ApiClient } from '../shared/api-client';

export const searchRequest = () => {
  return {
    type: ACTION_TYPES.SEARCH.SEARCH_REQUEST
  };
};

export const searchSuccess = (venues) => {
  return {
    type: ACTION_TYPES.SEARCH.SEARCH_SUCCESS,
    venues
  };
};

export const searchFailure = (error) => {
  return {
    type: ACTION_TYPES.SEARCH.SEARCH_FAILURE,
    error
  };
};

export const search = (latitude, longitude, radius) => {
  return (dispatch) => {
    dispatch(searchRequest());
    return new Promise((resolve, reject) => {
      return ApiClient.get(`search?lat=${latitude}&lng=${longitude}&radius=${radius}`)
        .then(
          response => {
            const venues = response.data.response.venues;
            dispatch(searchSuccess(venues));
            resolve(response);
          },
          error => {
            dispatch(searchFailure(error));
          }
        );
    });
  };
};