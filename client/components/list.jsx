import _ from 'lodash';
import React from 'react';

import ListItem from './list-item';
import PostcodeSearch from './postcode-search';

import styles from './list.scss';

type Props = {
  dispatch: () => void,
  venues: array,
  mapCenterFunc: func
}

class List extends React.Component {
  props: Props;

  venues = () => {
    return _.map(this.props.venues, (venue, idx) => {
      return (
        <ListItem
          key={venue.id}
          venue={venue}
          index={idx}
          mapCenterFunc={this.props.mapCenterFunc}
        />
      );
    });
  }

  postcodeSearch = () => {
    return <PostcodeSearch />;
  }

  render() {
    if (!this.props.venues) {
      return false;
    }

    if(this.props.venues.length < 1) {
      return (
        <div className={styles.noResults}>
          {this.postcodeSearch()}
          <p>
            <span className={styles.sadFace}>
              😕
            </span>
            No results here
          </p>
        </div>
      );
    }

    return (
      <div className={styles.list}>
        {this.postcodeSearch()}
        {this.venues()}
      </div>
    )
  }
}

export default List;