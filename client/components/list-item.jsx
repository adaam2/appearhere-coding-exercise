import _ from 'lodash';
import React from 'react';

import styles from './list-item.scss';

type Props = {
  dispatch: () => void,
  venue: object,
  index: number,
  mapCenterFunc: func
}

class ListItem extends React.Component {
  props: Props;

  trackToMarker = () => {
    const location = this.props.venue.location;
    this.props.mapCenterFunc(
      location.lat,
      location.lng
    );
  }

  render() {
    if (!this.props.venue) {
      return false;
    }

    return (
      <div 
        className={styles.listItem}
        onClick={this.trackToMarker}
      >
          <div className={styles.markerContainer}>
            <span className="marker">{this.props.index + 1}</span>
          </div>
          <div className={styles.contentContainer}>
            <h2>
              {this.props.venue.name}
              <span className={styles.faded}>
                {`${this.props.venue.location.address}, ${this.props.venue.location.city}`}
              </span>
            </h2>
            <p className={styles.faded}>
              <span className={styles.label}># of check-ins:</span>
              {this.props.venue.stats.checkinsCount}
            </p>
          </div>
      </div>
    )
  }
}

export default ListItem;