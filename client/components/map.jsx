import _ from 'lodash';
import React from 'react';
import { connect }  from 'react-redux';
import GoogleMap from 'google-map-react';

import Venue from './venue';
import List from './list';

import { calculateRadius } from 'shared/geometry';

import { geocodeSuccess } from 'actions/geocode';
import { search } from 'actions/search';

import { getVenuesOrderedByPopularity } from 'selectors/venue';
import { getCoordinates } from 'selectors/geocode';

import styles from './map.scss';

type Props = {
  dispatch: () => void,
  center: object,
  venues: array
}

class Map extends React.Component {
  props: Props;

  static defaultProps = {
    center: {
      lat: 51.508530,
      lng: -0.076132
    },
    venues: []
  };

  componentDidMount() {
    this.props.dispatch(search(this.props.center.lat, this.props.center.lng));
  }

  onChange = (data) => {
    const radius = calculateRadius(data);
    this.props.dispatch(search(data.center.lat, data.center.lng, radius));
  }

  venues = () => {
    return _.map(this.props.venues, (venue, idx) => {
      return (
        <Venue
          key={venue.id}
          data={venue}
          index={idx}
          lat={venue.location.lat}
          lng={venue.location.lng}
        />
      );
    });
  }

  setMapCenter = (lat, lng) => {
    this.props.dispatch(geocodeSuccess([lat, lng]));
  }

  map = () => {
    return (
      <div className={styles.map}>
        <GoogleMap
          className={styles.map}
          center={this.props.center}
          defaultZoom={15}
          onChange={this.onChange}
          minZoomOverride={true}
          minZoom={13}
        >
          {this.venues()}
        </GoogleMap>
      </div>
    );
  }

  list = () => {
    return (
      <List 
        mapCenterFunc={this.setMapCenter}
        venues={this.props.venues}
      />
    );
  }

  render() {
    if (!this.props.venues) {
      return false;
    }

    return (
      <div className={styles.mapContainer}>
        {this.map()}
        {this.list()}
      </div>
    )
  }
}

export default connect(store => {
  return {
    venues: getVenuesOrderedByPopularity(store),
    center: getCoordinates(store)
  };
})(Map);