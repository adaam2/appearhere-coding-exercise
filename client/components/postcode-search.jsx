import React from 'react';
import { connect }  from 'react-redux';

import { search } from 'actions/search';
import { geocode } from 'actions/geocode';

import styles from './postcode-search.scss';

type Props = {
  dispatch: () => void
}

class PostcodeSearch extends React.Component {
  props: Props;

  constructor(props) {
    super(props);
    
    this.state = {
      query: ''
    };
  }

  searchByPostcode = (evt) => {
    const postcode = evt.target.value;
    this.setState({
      query: postcode
    }, () => {
      this.props.dispatch(
        geocode(postcode)
      );
    }); 
  }

  render() {
    return (
      <div className={styles.postcodeSearch}>
        <input
          type="text"
          value={this.state.query}
          className={styles.postcodeInput}
          onChange={this.searchByPostcode}
          placeholder="Search by postcode, enter to submit"
        />
      </div>
    )
  }
}

export default connect()(PostcodeSearch);