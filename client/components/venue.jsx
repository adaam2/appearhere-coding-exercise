import React from 'react';

import { search } from 'actions/search';

import styles from './venue.scss';

type Props = {
  dispatch: () => void,
  data: object,
  index: number
}

class Venue extends React.Component {
  props: Props;

  constructor(props) {
    super(props);
    
    this.state = {
      hovered: false
    };
  }

  toggleHover = () => {
    this.setState({
      hovered: !this.state.hovered
    });
  }

  venueDetails = () => {
    if (!this.state.hovered) {
      return null;
    }

    return (
      <div className={styles.venueDetails}>
        {this.props.data.name}
      </div>
    );
  }

  render() {
    if (!this.props.data) {
      return false;
    }

    return (
      <div
        onMouseEnter={this.toggleHover}
        onMouseLeave={this.toggleHover}
        className="marker"
      >
        {this.props.index + 1}
        {this.venueDetails()}
      </div>
    )
  }
}

export default Venue;