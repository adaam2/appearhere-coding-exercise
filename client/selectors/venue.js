import _ from 'lodash';
import { createSelector } from 'reselect';

export const getVenueStore = store => store.search;

export const getVenues = createSelector(
  [getVenueStore],
  (venueStore) => {
    return _.get(venueStore, ['venues']);
  }
);

export const getVenuesOrderedByPopularity = createSelector(
  [getVenues],
  (venues) => {
    return _.sortBy(venues, (v) => { return v.stats.checkinsCount; }).reverse();
  }
);