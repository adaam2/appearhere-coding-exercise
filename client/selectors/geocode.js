import _ from 'lodash';
import { createSelector } from 'reselect';

export const getGeocodeStore = store => store.geocode;

export const getCoordinates = createSelector(
  [getGeocodeStore],
  (geocodeStore) => {
    return _.get(geocodeStore, ['latlng']);
  }
);
