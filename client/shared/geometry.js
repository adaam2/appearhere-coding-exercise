export const calculateRadius = (data) => {
  // r = radius of the earth in km
  const r = 6378.8
  // degrees to radians (divide by 57.2958)
  const ne_lat = data.bounds.ne.lat / 57.2958
  const ne_lng = data.bounds.ne.lng / 57.2958
  const c_lat = data.center.lat / 57.2958
  const c_lng = data.center.lng / 57.2958
  // distance = circle radius from center to Northeast corner of bounds
  const r_km = r * Math.acos(
    Math.sin(c_lat) * Math.sin(ne_lat) + 
    Math.cos(c_lat) * Math.cos(ne_lat) * Math.cos(ne_lng - c_lng)
    )
  return r_km *1000 // radius in meters
}