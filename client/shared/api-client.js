import request from 'axios';

class ApiClient {
  constructor() {
    this._apiHost = '/api';
    this._requestUrl = '';
  }

  get(url, params = {}) {
    this._requestUrl = `${this._apiHost}/${url}`;
    return request.get(this._requestUrl, params);
  }

  post(url, params = {}) {
    this._requestUrl = `${this._apiHost}/${url}`;
    return request.post(this._requestUrl, params);
  }
}

export default new ApiClient();