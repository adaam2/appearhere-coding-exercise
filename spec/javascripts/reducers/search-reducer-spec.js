import reducer from 'reducers/search';
import * as actions from 'actions/search';

describe('search reducer', () => {
  let state;
  const venues = [
    {
      id: 1,
      name: 'Venue 1'
    }
  ];

  beforeEach(() => {
    jest.resetModules();
  });

  describe('SEARCH_REQUEST', () => {
    it('should set isLoading to true', () => {
      state = reducer(undefined, actions.searchRequest());
      expect(state.isLoading).toEqual(true);
    });
  });

  describe('SEARCH_SUCCESS', () => {
    it('should set the venues array and is loading to false', () => {  
      state = reducer(undefined, actions.searchSuccess(venues));
      expect(state.venues[0]).toEqual(venues[0]);
      expect(state.isLoading).toEqual(false);
    });
  });

  describe('SEARCH_FAILURE', () => {
    it('should set isLoading to false, and the error to the relevant object', () => {
      state = reducer(undefined, actions.searchFailure({
        message: 'There is an error'
      }));
      expect(state.error.message).toEqual('There is an error');
      expect(state.isLoading).toEqual(false);
    });
  });
});