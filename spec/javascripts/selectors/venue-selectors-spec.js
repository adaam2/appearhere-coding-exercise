import _ from 'lodash';
import * as VenueSelectors from 'selectors/venue';

const store = {
  venues: [
    {
      id: 1,
      name: 'Adams venue',
      stats: {
        checkinsCount: 1
      }
    },
    {
      id: 2,
      name: 'Another venue',
      stats: {
        checkinsCount: 5
      }
    }
  ]
};

describe('venue selectors', () => {
  describe('getVenues', () => {
    it('should return the correct part of the redux store', () => {
      expect(VenueSelectors.getVenues.resultFunc(store)).toEqual(store.venues);
    });
  });

  describe('getVenuesOrderedByPopularity', () => {
    it('should return the venues in the correct order', () => {
      const actual = _.map(VenueSelectors.getVenuesOrderedByPopularity.resultFunc(store.venues), venue => venue.id);
      expect(actual).toEqual([2, 1]);
    });
  });
});
