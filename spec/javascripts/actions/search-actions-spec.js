jest.mock('shared/api-client');

import thunk from 'redux-thunk';
import { ACTION_TYPES } from 'constants/action-types';
import configureStore from 'redux-mock-store';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

const lat = 51.508530;
const lng = -0.076132;
const radius = 800;

describe('search actions', () => {
  let store;
  let apiClient;
  let actions;

  beforeEach(() => {
      jest.resetModules();
      store = mockStore();
      apiClient = require('shared/api-client');
      actions = require('actions/search');
    });

  describe('searchRequest', () => {
    it('should create an action to SEARCH_REQUEST', () => {
      const expectedAction = {
        type: ACTION_TYPES.SEARCH.SEARCH_REQUEST,
      };

      expect(actions.searchRequest()).toEqual(expectedAction);
    });
  });

  describe('searchSuccess', () => {
    it('should create an action to SEARCH_SUCCESS', () => {
      const expectedAction = {
        type: ACTION_TYPES.SEARCH.SEARCH_SUCCESS,
        venues: []
      };

      expect(actions.searchSuccess([])).toEqual(expectedAction);
    });
  });

  describe('search action creator', () => {
    let response;

    beforeEach(() => {
      response = {
        data: {
          response: {
            venues: [
              {
                id: 1,
                name: 'Venue'
              }
            ]
          }
        }
      }
      apiClient.get = jest.fn().mockReturnValue(
        () => Promise.resolve()
      )
      actions.searchSuccess = jest.fn();
    });

    it('should call the api endpoint', () => {
      store.dispatch(actions.search(lat, lng, radius)).then((result) => {
        expect(apiClient.get).toBeCalledWith(`/api/search?lat=${latitude}&lng=-${longitude}&radius=${radius}`);
      });
    });

    it('should trigger the search success event', () => {
      store.dispatch(actions.search(lat, lng, radius))
      .then(
        result => {
          const actual = result.data.response.venues;
          expect(actions.searchSuccess).toBeCalledWith(actual);
          expect(result).toEqual(actual);
        }
      );
    });
  });
});
