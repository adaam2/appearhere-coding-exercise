require 'rack/test'

describe 'API application' do
  include Rack::Test::Methods

  def app
    Server.new
  end

  it 'displays the API home page' do 
    get '/'
    expect(last_response.status).to eq 200
    expect(last_response.body).to include('This is the homepage of the API')
  end
end
