require 'geocoder'
require 'geocoder/results/base'
require 'geocoder/results/google'

describe Foursquare::Geocoder, type: :service do
  let(:postcode) { 'AL1 3UG' }

  let!(:lat) { 51.508530 }
  let!(:lng) { -0.3993 }
  let(:coordinates) {
    {
      lat: lat,
      lng: lng
    }
  }

  let(:geocoding_data) {
    {
      geometry: {
        location: coordinates
      }
    }
  }
  let(:good_geocoding_result) { double ::Geocoder::Result::Google, data: geocoding_data }
  let(:bad_geocoding_result) { double ::Geocoder::Result::Google, data: {} }
  let(:subject) { described_class.new }

  describe '#search' do
    context 'Valid postcode' do
      before do
        allow(::Geocoder)
          .to receive(:search)
          .and_return([good_geocoding_result])
      end

      it 'should return the correct array of lat / lng' do
        result = subject.search(postcode)
        expect(result[:lat]).to eq coordinates[:lat]
        expect(result[:lng]).to eq coordinates[:lng]
      end
    end

    context 'Invalid postcode / Empty string' do
      before do
        allow(::Geocoder)
          .to receive(:search)
          .and_return([bad_geocoding_result])
      end

      it 'should return an empty coordinate array' do
        expect(subject.search(postcode)).to be_empty
      end
    end
  end
end
