require 'sinatra_helper'

describe Foursquare::Service, type: :service do
  describe '#search' do
    let(:latitude) { '51.508530' }
    let(:longitude) { '-0.076132' }
    let(:radius) { 800 }
    let(:subject) {
      described_class.new(
        key: 'some fake key',
        secret: 'some fake secret',
        base: 'https://api.foursquare.com/v2/'
      )
    }
    let(:response) { File.read('./spec/support/fixtures/response.json') }

    before do
      allow_any_instance_of(Faraday::Connection)
        .to receive(:get)
        .and_return(
          double('response', status: 200, body: response)
        )
    end

    it 'should return the correct return value' do
      expect(subject.search(latitude, longitude, radius)).to eq JSON.parse(response)
    end
  end
end
