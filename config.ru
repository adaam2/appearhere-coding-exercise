require 'require_all'
require 'dotenv'

# Require all files in the server dir
require_all './server'

# Load in the environment variables from .env
Dotenv.load

puts 'Loaded environment variables'

# The Sinatra app is namespaced to /api, whereas the react app sits at the root /
run Rack::URLMap.new(
  '/api' => Server,
  '/' => WebpackProxy.new
)
