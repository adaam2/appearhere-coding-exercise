module Environment
  def fetch(name)
    ENV.fetch(name)
  end
end
