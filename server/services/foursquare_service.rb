module Foursquare
  class Service
    def initialize(opts)
      @api_key = opts[:key]
      @api_secret = opts[:secret]
      @base_url = opts[:base]
      @api_version = '20170816'
      @conn = Faraday.new(url: @base_url) do |faraday|
        faraday.response :logger, ::Logger.new(STDOUT), bodies: true
        faraday.adapter Faraday.default_adapter
      end
    end

    def search(latitude, longitude, radius)
      params = {
        ll: latlng(latitude, longitude),
        limit: 50,
        radius: radius,
        intent: 'browse',
        categoryId: '4bf58dd8d48988d1e0931735' # the coffee category
      }
      response = send_request('venues/search', 'GET', params)
      JSON.parse(response.body)
    end

    private

    def latlng(latitude, longitude)
      "#{latitude},#{longitude}"
    end

    def authentication
      {
        client_id: @api_key,
        client_secret: @api_secret,
        v: @api_version
      }
    end

    def send_request(path, method, params)
      request = @conn.public_send(method.downcase) do |req|
        req.url path
        req.params = params.merge(authentication)
      end
      yield request if block_given?
      request
    end
  end
end
