require 'active_support/all'

module Foursquare
  class Geocoder
    def search(postcode)
      first_result(postcode) || {}
    end

    private

    def latlng(result)
      result.data.deep_symbolize_keys!
      location = result.data[:geometry][:location]
      {
        lat: location[:lat],
        lng: location[:lng]
      }
    end

    def first_result(postcode)
      results = ::Geocoder.search(postcode)
      return if results.first.data.empty?
      latlng(results.first)
    end
  end
end
