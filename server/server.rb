require 'sinatra/base'
require 'faraday'
require 'geocoder'
require_relative './routes'
require 'sinatra/json'

# Ruby buffers stdout by default. Make it sync
$stdout.sync = true

class Server < Sinatra::Base
  include Routing
end
