require 'sinatra/json'
require_relative './services/env_service'
require_relative './services/foursquare_service'

module Routing
  include Environment

  def self.included(app)
    app.get '/' do
      erb :home
    end

    app.get '/search' do
      json foursquare_service.search(params[:lat], params[:lng], params[:radius])
    end

    app.post '/geocode' do
      json geocoding_service.search(params[:postcode])
    end
  end
 
  private

  def foursquare_service
    Foursquare::Service.new(
      key: fetch('FOURSQUARE_API_KEY'),
      secret: fetch('FOURSQUARE_API_SECRET'),
      base: fetch('FOURSQUARE_BASE_URL')
    )
  end

  def geocoding_service
    Foursquare::Geocoder.new
  end
end
